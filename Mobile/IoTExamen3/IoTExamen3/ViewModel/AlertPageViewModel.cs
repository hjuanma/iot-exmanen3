﻿using IoTExamen3.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;

namespace IoTExamen3.ViewModel
{
   public class AlertPageViewModel : INotifyPropertyChanged
   {
      public ObservableCollection<Alert> Alerts { get; set; } = new ObservableCollection<Alert>();

      public event PropertyChangedEventHandler PropertyChanged;
      private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
      {
         PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }


   }
}
