﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTExamen3.View
{
   [XamlCompilation(XamlCompilationOptions.Compile)]
   public partial class ShellHeader : ContentView
   {
      public ShellHeader()
      {
         InitializeComponent();
      }
   }
}