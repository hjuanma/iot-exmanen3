﻿
using IoTExamen3.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTExamen3
{
   [XamlCompilation(XamlCompilationOptions.Compile)]
   public partial class AlertPage : ContentPage
   {

      AlertPageViewModel vm;
      public AlertPage()
      {
         InitializeComponent();
         BindingContext = vm = new AlertPageViewModel();
      }
   }
}