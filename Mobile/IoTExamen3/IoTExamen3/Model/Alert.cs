﻿using Newtonsoft.Json;

namespace IoTExamen3.Model
{
   public class Alert
   {
      [JsonProperty("id")]
      public int Id { get; set; }

      [JsonProperty("value")]
      public float? Value { get; set; }

      [JsonProperty("longitude")]
      public float Longitude { get; set; }

      [JsonProperty("latitude")]
      public float Latitude { get; set; }

      [JsonProperty("category")]
      public int Category { get; set; }

      [JsonProperty("color")]
      public string Color { get; set; }

   }
}
