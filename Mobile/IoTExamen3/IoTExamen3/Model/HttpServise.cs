﻿using IoTExamen3.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IoTExamen3.Model
{
   public class HttpServise
   {

      private HttpClient client;


      public HttpServise()
      {
         client = new HttpClient();
      }

      public async Task<List<Alert>> RefreshDataAsync()
      {
         List<Alert> items = new List<Alert>();

         Uri uri = new Uri(string.Format(Constants.ServerUrl, string.Empty));

         HttpResponseMessage response = await client.GetAsync(uri);
         if (response.IsSuccessStatusCode)
         {
            items = JsonConvert.DeserializeObject<List<Alert>>(await response.Content.ReadAsStringAsync());
         }

         return items;
      }
   }
}
