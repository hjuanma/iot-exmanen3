﻿using Xamarin.Essentials;

namespace IoTExamen3.Model
{
   public static class Location
   {
      public static double Latitude
      {
         get => Preferences.Get(nameof(Latitude), float.PositiveInfinity);
         set => Preferences.Set(nameof(Latitude), value);
      }

      public static double Longitude
      {
         get => Preferences.Get(nameof(Longitude), float.PositiveInfinity);
         set => Preferences.Set(nameof(Longitude), value);
      }
   }
}
