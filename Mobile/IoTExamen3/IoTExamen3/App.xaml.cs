﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace IoTExamen3
{
   public partial class App : Application
   {
      public App()
      {
         InitializeComponent();

         MainPage = new SellApp();
      }

      protected override void OnStart()
      {
      }

      protected override void OnSleep()
      {
      }

      protected override async void OnResume()
      {
         try
         {
            GeolocationRequest request = new GeolocationRequest(GeolocationAccuracy.Medium);
            Location location = await Geolocation.GetLocationAsync(request);

            if (location != null)
            {
               global::IoTExamen3.Model.Location.Latitude = location.Latitude;
               global::IoTExamen3.Model.Location.Longitude = location.Longitude;
            }
         }
         catch (FeatureNotSupportedException fnsEx)
         {
            // Handle not supported on device exception
            Console.WriteLine($"{fnsEx.GetType().Name} - {fnsEx.Message}");
         }
         catch (FeatureNotEnabledException fneEx)
         {
            // Handle not enabled on device exception
            Console.WriteLine($"{fneEx.GetType().Name} - {fneEx.Message}");
         }
         catch (PermissionException pEx)
         {
            // Handle permission exception
            Console.WriteLine($"{pEx.GetType().Name} - {pEx.Message}");
         }
         catch (Exception ex)
         {
            // Unable to get location
            Console.WriteLine($"{ex.GetType().Name} - {ex.Message}");
         }
      }
   }
}
