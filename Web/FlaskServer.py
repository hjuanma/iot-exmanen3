#Importacion de paquetes necesarios para la ejecucion del codigo
from flask import Flask, render_template, request, url_for, redirect, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, timedelta

import pandas as pd
import Model
from Model import PollutantType

server = Flask(__name__)

#Se ingresa la referencia a la db que se usara, ya sea sqlite o Mysql
server.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/iot.db'

db = SQLAlchemy(server)


url = "http://siata.gov.co:8089/estacionesAirePM25/cf7bb09b4d7d859a2840e22c3f3a9a8039917cc3/"

###### MODELOS DE DATOS PARA LA DB Y MANEJO DE OBJETOS EN EL CODIGO ######
###### Estos modelos tambien los usara de referencia el FrameWork flask para crear las tablas y relaciones de la db ######

#Se crea un modelo para los objetos 'Records' los que tendran el registro de la los datos enviado por los sensores
class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    date = db.Column(db.DateTime)

    def serialize(self):
        return {
            'id': str(self.id),   
            'value' : self.value,
            'latitud': self.latitude,
            'longitud': self.longitude,
            'fecha' : self.date,
        }

#Se crea un modelo para los objetos 'Calculated' los que tendran el registro de la los datos enviado por los sensores
class Calculated(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    category = db.Column(db.Integer)
    color = db.Column(db.String)

    def serialize(self):
        return {
            'id': str(self.id),   
            'value' : self.value,
            'latitud': self.latitude,
            'longitud': self.longitude,
            'categoria' : self.category,
            'color' : self.color,
        }

##### RECORDS #####
# se consultan todos los registros que se tienen hasta el momento y se envian a la ruta /records para ser pitados en la interfaz
@server.route('/records')
def records():
    return  jsonify([r.serialize() for r in GetRecordList()])

    
@server.route('/aqi')
def calculated():    
    lat  = request.args.get('lat', None)
    lon  = request.args.get('lon', None)
    record = Calculated.query.filter(Calculated.latitude == lat and Calculated.longitude == lon).first()
    if record is not None:
        return Response(record.serialize(), mimetype='application/json')
    else:
        response = jsonify({'message': 'Error'})
        response.status_code = -1
        return response

def GetRecordList():
    return Record.query.all()

def GetCalculatesList():
    return Calculated.query.all()


def GetJson():
    captura_web = pd.read_json(url,convert_dates='True')
    return captura_web.datos.values.tolist()

def SaveCalculate():
    
    grid, gridx, gridy = Model.CreateGrid(GetRecordList())

    lat = []
    lon = []
    val = []

    for i in range(len(gridx)):
         for j in range(len(gridx)):
            lat.append(gridx[i][j])
            lon.append(gridy[i][j])
            val.append(Model.CalculateAQI(grid[i][j], PollutantType.PM25))

    for i in range(len(lon)):
        
        valor = val[i]
        latitud = lat[i]
        longitud = lon[i]
        cat, color = Model.CalculateCategory(valor)

        if cat != 0:

            calc = Calculated( value = valor, longitude = longitud, latitude = latitud, category = cat, color = color)
            db.session.add(calc)
            
    db.session.commit()

def SaveData(data):    
    modificated = False
    for i in range(len(data)):
        dato = data[i]['valorICA']

        #Se valida que los dos cumplan con unos estandares minimos para se utilizables
        if(Model.ValidateData(dato)):
            #Se realiza un tratamiento de datos para determinar indice de contaminacion en el punto
            date = data[i]['ultimaActualizacion']
            fecha =  datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
            latitud = data[i]['coordenadas'][0]['latitud']
            longitud = data[i]['coordenadas'][0]['longitud']
            value = dato

            record = Record(latitude = latitud, longitude = longitud, value = value, date =  fecha)

            #Valida si el registro ya existe
            exists = db.session.query(db.exists().where(Record.latitude == record.latitude and  Record.longitude ==  record.longitude and Record.date == record.date)).scalar()

            #En caso de no existir lo agrega a la base de datos
            if not exists:
                db.session.add(record)
                modificated = True
    #Se valida se tienen nuevos registos para subir            
    if modificated:
        db.session.commit()
        SaveCalculate()
