#Importacion de paquetes necesarios para la ejecucion del codigo
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import plotly.express as px

#se importa la clase flaskServer como servidor base
from FlaskServer import server, Record, db
import FlaskServer as fs

app = dash.Dash(server = server,
routes_pathname_prefix = '/map/')

fs.SaveData(fs.GetJson())

records = fs.GetCalculatesList()

px.set_mapbox_access_token('pk.eyJ1IjoibGVvbmFyZG9iZXRhbmN1ciIsImEiOiJjazlybGNiZWcwYjZ6M2dwNGY4MmY2eGpwIn0.EJjpR4klZpOHSfdm7Tsfkw')

fig1={
        'data': [{
            'lat': [r.latitude for r in records],
            'lon': [r.longitude for r in records],
            'marker': {
                'color': [r.color for r in records],
                'size': 10,
                'opacity': .4
            },
            'customdata': [r.value for r in records],
            'type': 'scattermapbox'
        }],
        'layout': {
            'mapbox': 
            {
                'accesstoken': 'pk.eyJ1IjoibGVvbmFyZG9iZXRhbmN1ciIsImEiOiJjazlybGNiZWcwYjZ6M2dwNGY4MmY2eGpwIn0.EJjpR4klZpOHSfdm7Tsfkw',
                'center' : 
                {
                    'lat': 6.240737,
                    'lon': -75.589900
                },
                'zoom' : 10,
                'style' : 'stamen-terrain'
            },
            'hovermode': 'closest',
            'margin': {'l': 0, 'r': 0, 'b': 0, 't': 0}
        }
    }

app.layout = html.Div([
        html.H1('Alerta AQI'),
        dcc.Graph(id='map', figure=fig1)
    ])


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=80)

